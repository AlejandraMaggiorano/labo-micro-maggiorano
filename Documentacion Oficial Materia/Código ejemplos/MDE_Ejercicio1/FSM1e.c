#define EST_A 0
#define EST_B 1
#define EST_C 2

#define ROJO 0
#define VERDE 1

int Estado_Sensor (void);
void Encender_Alarma (void);
void Apagar_Alarma (void);
int Medicion_Sensor (void);
void Encender_Luz (int color);
void Apagar_Luz (int color);

typedef enum {EST_A, EST_B, EST_C} t_estado_FSM_sensor;

t_estado_FSM_sensor estado_FSM_sensor;

int Inicializar_FSM_Sensor (void)
{	
	estado_FSM_sensor = EST_A;
	Apagar_Alarma();
	Apagar_Luz(ROJO);
	Apagar_Luz(VERDE);
	
	return 0;
}

int FSM_Sensor (void)
{
	
	switch(estado_FSM_sensor)
	{
		case EST_A:
			// TRANSICIONES DE EST_A
			if(Estado_Sensor() == 0)
			{
				estado_FSM_sensor = EST_A;
				Encender_Alarma();
			}
			else if (Estado_Sensor == 1)
			{
				estado_FSM_sensor = EST_B;
				Apagar_Alarma();			
			}					
			
			break;
			
		case EST_B:
			// TRANSICIONES DE EST_B
			
			if(Estado_Sensor() == 0)
			{
				Encender_Alarma();
				Apagar_Luz(VERDE);
				estado_FSM_sensor = EST_A;
			}
			else if (Medicion_Sensor() < 100)
			{
				Encender_Luz(VERDE);
				estado_FSM_sensor = EST_B;
			}
			else if (Medicion_Sensor() >= 100)
			{
				Encender_Luz(ROJO);
				Apagar_Luz(VERDE);
				estado_FSM_sensor = EST_C;				
			}			
			break;
			
		case EST_C:
			// TRANSICIONES DE EST_C
			if(Estado_Sensor() == 0)
			{
				Encender_Alarma();
				Apagar_Luz(ROJO);
				estado_FSM_sensor = EST_A;
			}
			else if (Medicion_Sensor() < 100)
			{
				Encender_Luz(VERDE);
				Apagar_Luz(ROJO);
				estado_FSM_sensor = EST_B;
			}
			else if (Medicion_Sensor() >= 100)
			{
				Encender_Luz(ROJO);
				Apagar_Luz(VERDE);
				estado_FSM_sensor = EST_C;				
			}			
			break;
		

		default:
			Inicializar_FSM_Sensor();
	
	}
	
}



