#define TVENTANA 	200
#include "antirreb.h"

typedef enum {ESTADO_ARRIBA, ESTADO_VENTANA, ESTADO_ABAJO} estado_t;

mdeBotonStruct boton1;
mdeBotonStruct boton2;


int32_t InicializarMdeAntirreb(mdeBotonStruct*pBoton) {
	InicializarBoton();
	pBoton->estadoAntirreb = ESTADO_ARRIBA;
	pBoton->presionBoton = 0;
	return 0;
}

int32_t mdeAntirreb(mdeBotonStruct*pBoton) {

switch(pBoton->estadoAntirreb) {

case ESTADO_ARRIBA:
	if(EstadoBoton(pBoton->nBoton)== 0){
		pBoton->cuenta_ms = 0;
		pBoton->estadoAntirreb = ESTADO_VENTANA;
	}
	break;

case ESTADO_VENTANA:
	if((pBoton->cuenta_ms >= TVENTANA) && (EstadoBoton(pBoton->nBoton) == 1)) {
		pBoton->estadoAntirreb = ESTADO_ARRIBA;
		if(pBoton->flagEstadoAbajo == true) {
			pBoton->flagEstadoAbajo = false;
			pBoton->presionBoton++;
		}		 
	}
	else if((pBoton->cuenta_ms >= TVENTANA) && (EstadoBoton(pBoton->nBoton) == 0)) {
		pBoton->estadoAntirreb = ESTADO_ABAJO;
		pBoton->flagEstadoAbajo = true;
	}

	break;
case ESTADO_ABAJO:
	if(EstadoBoton(pBoton->nBoton)== 1){
		pBoton->cuenta_ms = 0;
		pBoton->estadoAntirreb = ESTADO_VENTANA;
	}	
	break;
default:
	InicializarMdeAntirreb(pBoton);

}

}




