
#define TIEMPO_VENTANA 300

typedef enum {B_SUELTO, B_PRES} t_boton;

t_boton LeerBoton(void);

int ActivarContadorMS (void);
int DesactivarContadorMS (void);
int cont;

typedef enum {SUELTO, VENT, PRES} t_mde_boton;

t_mde_boton mde_boton_estado;

int Inicializar_MDE_Boton (void)
{
	mde_boton_estado = SUELTO;
	DesactivarContadorMS();
	cont = 0;
}

int MDE_Boton (void)
{
	switch(mde_boton_estado)
	{
		case SUELTO:
			
			if(LeerBoton() == B_SUELTO)
			{
				mde_boton_estado = SUELTO;						
			}
			else if (LeerBoton() == B_PRES)
			{
				mde_boton_estado = VENT;
				cont = 0;
				ActivarContadorMS();								
			}
		
			break;
			
		case VENT:
			
			if(cont < TIEMPO_VENTANA)
			{
				mde_boton_estado = VENT;
			}
			else if((cont > TIEMPO_VENTANA) && (LeerBoton() == B_PRES))
			{
				mde_boton_estado = PRES;
				DesactivarContadorMS();
				cont = 0;
			}
			else if((cont > TIEMPO_VENTANA) && (LeerBoton() == B_SUELTO))
			{
				mde_boton_estado = SUELTO;
				DesactivarContadorMS();
				cont = 0;
			}
			
			break;
			
		case PRES:
		
			if (LeerBoton() == B_PRES)
			{
				mde_boton_estado = PRES;
			}
			else if (LeerBoton() == B_SUELTO)
			{
				mde_boton_estado = VENT;
				cont = 0;			
				ActivarContadorMS();
			
			}
		
			break;
			
		default:
			Inicializar_MDE_Boton();				
			return -1;
	}	
	return 0;
}


