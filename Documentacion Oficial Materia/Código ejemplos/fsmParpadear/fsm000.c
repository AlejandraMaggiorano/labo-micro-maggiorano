#include "hal.h"


/** tiempo apagado y encendido */
#define tA	1000
#define tE	500

/*
#define ESTADO_APAGADO		0
#define ESTADO_ENCENDIDO	1

static int estadoFsm;
*/

typedef enum {ESTADO_APAGADO, ESTADO_ENCENDIDO} estado_t;

static estado_t estadoFsm;


/* ---------- prototipos ----------- */

int inicializarFsm(void);
int fsm(void);

/* ---------- funciones ----------- */

/**
 *
 */
int inicializarFsm(void) {

	estadoFsm = ESTADO_APAGADO;
	cuentaTimer = 0;
	apagarLed();
	return 0;

}


/**
 *
 */
int fsm (void) {

	switch(estadoFsm) {

	case ESTADO_APAGADO:
		if(cuentaTimer < tA)
			estadoFsm = ESTADO_APAGADO;
		else if(cuentaTimer >= tA) {
			estadoFsm = ESTADO_ENCENDIDO;
			cuentaTimer = 0;
			encenderLed();
		}

		break;

	case ESTADO_ENCENDIDO:

		if(cuentaTimer < tE) {
			estadoFsm = ESTADO_ENCENDIDO;
		}
		else if(cuentaTimer >= tE) {
			estadoFsm = ESTADO_APAGADO;
			cuentaTimer = 0;
			apagarLed();
		}

		break;

	default:
		inicializarFsm();
	}
	return 0;
}
