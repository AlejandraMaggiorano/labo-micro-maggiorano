#ifndef EJ6_H_INCLUDED
#define EJ6_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#define LONG_COLA 10
struct colaDatos_t
{
    char datos[LONG_COLA];
    uint8_t longitudMax;
    uint8_t longitud;
};
void generarCola();
char getDato();
void addDato (char d);
void mostrarCola();
#endif // EJ6_H_INCLUDED
