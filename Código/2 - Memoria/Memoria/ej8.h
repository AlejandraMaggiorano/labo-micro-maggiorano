#ifndef EJ8_H_INCLUDED
#define EJ8_H_INCLUDED
#define LONG_PINGPONG 7

/*Implementar el c�digo para manejar un doble buffer (ping-pong buffer) y un programa de prueba
que permita almacenar datos e imprimir el buffer de almacenamiento cada vez que se llena. */

struct bufferPingPong_t
{
    char datos1[LONG_PINGPONG];
    char datos2[LONG_PINGPONG];
    uint8_t punteroPP;
    uint8_t bufferActivoPP;

};
void generarBPP();
void addDatoBPP (char d);
void mostrarBPP();

#endif // EJ8_H_INCLUDED
