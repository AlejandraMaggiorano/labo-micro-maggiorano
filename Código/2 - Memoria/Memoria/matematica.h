#ifndef EJ9_H_INCLUDED
#define EJ9_H_INCLUDED
/* Realizar una función que me permita realizar la
sumatoria de un array de enteros con signo de 16 bits y
devuelva un entero de 32 bits. Debe recibir la dirección
del array a sumar y su largo. El prototipo es el siguiente:
 int32_t ​Sumar_Array (​int16_t* ​x, ​int16_t ​xn); */

 int32_t SumarArray (int16_t* x, int16_t xn);


#endif // EJ9_H_INCLUDED
