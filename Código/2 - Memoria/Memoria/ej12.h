#ifndef EJ12_H_INCLUDED
#define EJ12_H_INCLUDED

/*
12)Agregar una función que devuelva un entero sin signo de 16 bits que me permita contar
la cantidad de veces que fue llamada la misma. Comprobar el lugar de memoria que
ocupan cada una de las variables creadas en el mapa general. El prototipo es el siguiente:

 uint16_t​ Cuenta_Accesos (​void​);

*/

uint16_t Cuenta_Accesos(void);
#endif // EJ12_H_INCLUDED
