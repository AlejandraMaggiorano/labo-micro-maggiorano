
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "ej7.h"

/* Implementar un set de funciones para manejo de una cola de datos FIFO de
variables del tamaño de un byte. Administrar todas las variables de la cola
de datos desde una estructura ​colaDatos_t.​
Realizar un programa de prueba que permita ingresar y mostrar datos a través de
las funciones implementadas.
7) Repetir el punto anterior para la implementación de buffers circulares. */
struct bufferCircular_t bufferCircular;
void generarBC()
{ //Genera el buffer circular
    bufferCircular.longitudMax = LONG_BUFFERC;
    bufferCircular.inicio=0;
    bufferCircular.fin=0;
}//generarBC

void incrementarFinBC()
{ // Incrementa el puntero de fin
     bufferCircular.fin++;
    if(bufferCircular.fin>=LONG_BUFFERC)
    {
         bufferCircular.fin = 0;
    }
}//incrementarFinBC

void incrementarInicioBC()
{ //Incremnta el puntero de inicio
     bufferCircular.inicio++;
    if(bufferCircular.inicio>=LONG_BUFFERC)
    {
         bufferCircular.inicio = 0;
    }
}//incrementarInicioBC

char getDatoBC()
{ //Devuelve el primer dato del buffer circular y lo borra
    char temp;
    if(bufferCircular.elementos>0)
    {
       temp= bufferCircular.datos[bufferCircular.inicio];
       incrementarInicioBC();
       bufferCircular.elementos--;
    }else temp=0;
    return temp;
}//getDatoBC

void addDatoBC (char d)
{ //Agrega un dato al buffer circular

    if(bufferCircular.elementos<LONG_BUFFERC)
    {
        bufferCircular.datos[bufferCircular.fin]=d;
        incrementarFinBC();
        bufferCircular.elementos++;
    }
}//addDatoBC

void mostrarBC()
{ // Muestra los valores del buffer circular

    for(int i=0; i<bufferCircular.elementos; i++)
    {
         printf("Valor %d del bufer circular:  %c \n",i,bufferCircular.datos[i]);
    }
}//mostrarBC
