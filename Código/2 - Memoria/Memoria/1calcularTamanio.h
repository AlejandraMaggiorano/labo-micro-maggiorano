#include <stdint.h>
#include <stdbool.h>

//Ejercicio 1
/* Realizar un programa que permita calcular el tama�o de un puntero a entero de 8 bits,
 * 16, 32 bits, float y double. Mostrar en pantalla cada uno de los tama�os. */

int calcularTamanioInt8( int8_t * valor );
int calcularTamanioInt16( int16_t * valor );
int calcularTamanioInt32( int32_t  valor );
int calcularTamanioFloat( float * valor );
int calcularTamanioDouble( double * valor );
