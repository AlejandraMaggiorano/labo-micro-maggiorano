#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "matematica.h"

/* Realizar una función que me permita realizar la
sumatoria de un array de enteros con signo de 16 bits y
devuelva un entero de 32 bits. Debe recibir la dirección
del array a sumar y su largo. El prototipo es el siguiente:
 int32_t ​Sumar_Array (​int16_t* ​x, ​int16_t ​xn); */

 int32_t SumarArray (int16_t* x, int16_t xn)
 { //Ver consigna
     int32_t temp=0;

     for(int i=0; i<xn; i++)
     {
         temp= temp + *(x+i);
         //printf ("Valor de x es %d \n",temp);
     }

      printf ("Valor de la suma es %d \n",temp);
      return temp;
 }//SumarArray
