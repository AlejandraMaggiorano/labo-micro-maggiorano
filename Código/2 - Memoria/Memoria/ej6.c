/* Implementar un set de funciones para manejo de una cola de datos FIFO
de variables del tamaño de un byte. Administrar todas las variables de
la cola de datos desde una estructura ​colaDatos_t.​
Realizar un programa de prueba que permita ingresar
y mostrar datos a través de las funciones implementadas. 7*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "ej6.h"


struct colaDatos_t cola;

void generarCola()
{
    cola.longitudMax = LONG_COLA;
    cola.longitud=0;

    printf("Longitud máxima de cola: %d\n",cola.longitudMax);
    printf("Longitud actual de cola: %d\n",cola.longitud);
}//generarCola

char getDato()
{ //Devuelve el primer dato y lo borra
    char temp =cola.datos[0];
    cola.longitud--;

    for (int i = 0; i<cola.longitud; i++)
    {
        cola.datos[i]=cola.datos[i+1];
    }

    return temp;
}//getDato

void addDato (char d)
{ //Agrega un nuevo dato al final de la cola
    if(cola.longitud<cola.longitudMax)
    {
    cola.datos[cola.longitud]=d;
    cola.longitud++;
    }
}//addDato

void mostrarCola()
{ //Muestra en pantalla el contenido de la cola

for (int i=0; i<cola.longitud; i++)
{
    printf("Valor %d de la cola:  %c \n",i,cola.datos[i]);
}

}//MostrarCola
