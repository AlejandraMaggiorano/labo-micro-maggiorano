#include <stdio.h>
#include <stdlib.h>
#include "1calcularTamanio.h"
#include "ej5.h"
#include "ej6.h"
#include "ej7.h"
#include "ej8.h"
#include "matematica.h"
#define LONG_COLA 10
 //extern char etext,edata, end;

int32_t suma(int32_t a, int32_t b);

int main()
{

    //Ejercicio 1
/* Realizar un programa que permita calcular el tamaño de un puntero a entero de 8 bits,
 * 16, 32 bits, float y double. Mostrar en pantalla cada uno de los tamaños. */

    int8_t puntero_int8 []= {5,4,5};
    int16_t puntero_int16 []= {5,32,5,432};
    int32_t puntero_int32 []= {5,4,5,0,183};
    float puntero_float[]= {5,4,5,5.64,543, 4.02};
    double puntero_double []= {5,4,5,4.32,4.555,6.5,6.543};

    /*Si le paso una dirección en memoria no sé la longitud del array.
    Si quisiera hacer una función tendría que pasarle la dirección del puntero y además la longitud como parámetro
    y multiplicar la longitud del primer elemento que es el único que considera por ese número.
    Acá está hecho de otra forma.
    */

    int a;
    a= sizeof(puntero_int8);
    printf("Valor Int8 : %d \n",a)/sizeof(puntero_int8[0]);
     a= sizeof(puntero_int16)/sizeof(puntero_int16[0]);
    printf("Valor Int16 : %d \n",a);
     a= sizeof(puntero_int32)/sizeof(puntero_int32[0]);
    printf("Valor Int32 : %d \n",a);
     a= sizeof(puntero_float)/sizeof(puntero_float[0]);
    printf("Valor Float : %d \n",a);
     a= sizeof(puntero_double)/sizeof(puntero_double[0]);
    printf("Valor Double : %d \n",a);

    //Fin ejercicio 1

//Ejercicio 2, 3 y 4
/*
Imprimir en pantalla el puntero ​etext. ​Que expresa este valor?
Realizar una función int32_t Suma(int32_t a, int32_t b)​ y generar un puntero que apunte a ella.
Imprimir esa dirección y la dirección del ​main​, comparar con el contenido de ​etext​.
*/

/*

    printf("First address past:\n");
    printf("    program text (etext)      %10p\n", __etext);
    printf("    initialized data (edata)  %10p\n", __edata);
    printf("    uninitialized data (end)  %10p\n", __end);
*/

/*Estas son las direcciones de memoria de:
etext: la primera dirección después del segmento del texto del programa
edata: la primera dirección después del final de los datos inicializados del programa
end: la primera dirección después de la memoria reservada (toda la memoria del programa)
Por algún motivo no me deja acceder a ellas, probablemente sea culpa del sistema operativo.
*/

//Ejercicio 5

int32_t v1;
v1 = 332;
int32_t v2;
v2 = 748;

printf("Valor v1 : %d \n", &v1);
printf("Valor v2 : %d \n", &v2);

suma(v1,v2);

//Fin ejercicio 5

//Ejercicio 6

generarCola();
mostrarCola();
/*addDato('c');
addDato('o');
addDato('l');
addDato('i');
addDato('t');
addDato('a');
mostrarCola();
getDato();
mostrarCola();*/

char aaa;

printf("Ingrese 10 caracteres y presione enter \n");
for(int i=0; i<LONG_COLA;i++)
{
 scanf("%c", &aaa);
 addDato(aaa);
}
mostrarCola();

//Fin Ejercicio 6

//Ejercicio 7

generarBC();
addDatoBC('r');
addDatoBC('i');
addDatoBC('n');
addDatoBC('g');
mostrarBC();
getDatoBC();
addDatoBC('e');
addDatoBC('n');
addDatoBC('i');
mostrarBC();
getDatoBC();
getDatoBC();
addDatoBC('i');
addDatoBC('o');
mostrarBC();

//Fin ejercicio 7
//Ejercico 8
/*printf("Ingrese 20 caracteres y presione enter \n");
for(int i=0; i<20;i++)
{
 scanf("%c", &aaa);
 addDatoBPP(aaa);
}*/

generarBPP();
addDatoBPP('b');
addDatoBPP('u');
addDatoBPP('f');
addDatoBPP('f');
addDatoBPP('e');
addDatoBPP('r');
addDatoBPP('1');
addDatoBPP('y');
addDatoBPP('-');
addDatoBPP('b');
addDatoBPP('u');
addDatoBPP('f');
addDatoBPP('f');
addDatoBPP('2');
addDatoBPP('2');

//Fin ejercicio 8

//Ejercicio 9
int16_t array_prueba[6] = {5 , 3 , 434 , 432,67,90};

SumarArray(&array_prueba,3);
SumarArray(&array_prueba,4);
SumarArray(&array_prueba,5);

//Ej 11 - Nunca anduvo el ejercicio 3

//Ej 12
printf("Accesos 1 : %d \n", Cuenta_Accesos());
printf("Accesos 2 : %d \n", Cuenta_Accesos());
printf("Accesos 3 : %d \n", Cuenta_Accesos());
printf("Accesos 4 : %d \n", Cuenta_Accesos());
printf("Accesos 5 : %d \n", Cuenta_Accesos());
printf("Accesos 6 : %d \n", Cuenta_Accesos());


return 0;
}
