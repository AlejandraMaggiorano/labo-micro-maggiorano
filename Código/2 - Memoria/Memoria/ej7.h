#ifndef EJ7_H_INCLUDED
#define EJ7_H_INCLUDED
#define LONG_BUFFERC 5



struct bufferCircular_t
{
    char datos[LONG_BUFFERC];
    uint8_t longitudMax;
    uint8_t fin;
    uint8_t inicio;
    uint8_t elementos;
};
void incrementarFinBC();
void incrementarInicioBC();
void generarBC();
char getDatoBC();
void addDatoBC (char d);
void mostrarBC();

#endif // EJ7_H_INCLUDED
