/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>

int main()
{
    
    float valor;
    //Valores por defecto
    float bit1=1; 
    float bit2=0;
    float bit3 = 1;
    ///Punto
    float bit_4 = 1;
    float bit_5 = 0;
    float bit_6 =1;
    float bit_7 = 0;
    float bit_8 = 1;
    //////
    
    valor= bit1*4+bit2*2+bit3*1+bit_4/2+bit_5/4+bit_6/8+bit_7/16+bit_8/32;
    valor= bit1*(-1)*(bit2*2+bit3*1+bit_4/2+bit_5/4+bit_6/8+bit_7/16+bit_8/32);
    printf("Número convertido: %f\n",valor);

/////////////////////////////////////////////////


 /// q7 x q7 - Devuelve resultado en q15
int q7_v1 = 0b10001011;
int q7_v2 = 0b01110111;

 printf("Valor 1 en Q7: %b\n",q7_v1);
 printf("Valor 2 en Q7: %b\n",q7_v2);
  
int16_t q15_resultado= q7_v1*q7_v2;
q15_resultado=q15_resultado<<1;
 printf("Valor resultado en Q15: %b\n",q15_resultado);
 
/////////////////////////////////////////////
/// q15 x q15 - Devuelve resultado en q31///

int16_t  q15_v1 = 0b1000111100110111;
int16_t q15_v2 = 0b0111011111001101;

 printf("Valor 1 en Q15: %b\n",q15_v1);
 printf("Valor 2 en Q15: %b\n",q15_v2);
  
int32_t q31_resultado= q15_v1*q15_v2;
q31_resultado=q31_resultado<<1;
 printf("Valor resultado en Q31: %b\n",q31_resultado);
 
return 0;
}

