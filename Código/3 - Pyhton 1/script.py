#print ("Primer script de python!")
import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd

#----SEÑAL --------
#------------------
presion= pd.read_csv("presion_tp1.csv")
 
Fs= 500
Ts= 1/Fs

vmin_sensor=-1
vmax_sensor=1

vmin_adt=0
vmax_adt=3.3
#------------------
#------------------

#Calcular ganancia G 
#Calcular offset

g1= vmax_adt-vmin_adt 
g2= vmax_sensor-vmin_sensor
G=g1/g2
print (G)
offset =  (vmax_adt-vmin_adt)/2
S2= (presion*G)+offset
n = len(presion)

k= np.arange(0,n)

t=k*Ts

presion_frec = abs(np.fft.fft(presion))
#-----------------------
#3) Simular la cuantificación de la señal con la función fix() de MATLAB, llevando el nivel de la
# señal S2 (0 a 3.3V) al rango de entrada de un ADC de N bits.
# ● Generar una señal D1 para un ADC de 8 bits, D2 para un ADC de 12 bits y D3 para un
# ADC de 24 bits.Graficar estas señales en tiempo y en frecuencia y comparar con lo
# graficado en el punto 2.
# ● Generar también un set de funciones E1, E2 y E3 con el error entre la señal en punto
# flotante las respectivas señales enteras D1, D2, D3. Graficar estas tres señales en
# tiempo y frecuencia.

E_Analog = presion - presion

#==== 8 bits ====
# 8 bits - va de -128 a 127
D1_escala = 2**8

#Escalamiento ( maxescala - minescala ) / ( maxseñal - minseñal )
D1_G = D1_escala/ g1
D1_analog = (S2 * D1_G)

#Cuantifico
D1_cuant = round(D1_analog)
print(D1_cuant)
#Error
E1 = D1_analog-D1_cuant


#==== 12 bits ====
# 12 bits - va de -2048 a 2047
D2_escala = 2**12

#Escalamiento ( maxescala - minescala ) / ( maxseñal - minseñal )
D2_G = D2_escala/ g1
D2_analog = (S2 * D2_G)

#Cuantifico
D2_cuant = round(D2_analog)
print(D2_cuant)
#Error
E2 = D2_analog-D2_cuant


#==== 24 bits ====
# 24 bits - va de -8,388,608 a 8,388,607
D3_escala = 2**24

#Escalamiento ( maxescala - minescala ) / ( maxseñal - minseñal )
D3_G = D3_escala/ g1
D3_analog = (S2 * D3_G)

#Cuantifico
D3_cuant = round(D3_analog)
#print(D3_cuant)

#Error
E3 = D3_analog-D3_cuant

#-----------------------
#------ PLOTEO ---------
#-----------------------

# ----- Punto 1 -----
plt.subplot(3,1,1)
plt.plot(t,presion)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("Grafico temporal")
plt.grid()

plt.subplot(3,1,2)
plt.plot(presion_frec)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("Grafico frecuencial")
plt.grid()

plt.subplot(3,1,3)
plt.plot(t,S2)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("Grafico frecuencial")
plt.grid()
plt.show()

# ----- Punto 2 -----

plt.subplot(4,1,1)
plt.plot(t,presion)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("Analog (pseudo)")
plt.grid()

plt.subplot(4,1,2)
plt.plot(t,D1_cuant)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("8 bits")
plt.grid()

plt.subplot(4,1,3)
plt.plot(t,D2_cuant)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("12 bits")
plt.grid()

plt.subplot(4,1,4)
plt.plot(t,D3_cuant)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("24 bits")
plt.grid()
plt.show()


# ---- Errores
plt.subplot(4,1,1)
plt.plot(t,E_Analog)
plt.xlabel("t[s]")
plt.ylabel("p [mmHG]")
plt.title("Analog (pseudo)")
plt.grid()

plt.subplot(4,1,2)
plt.plot(t,E1)
plt.xlabel("t[s]")
plt.ylabel("error")
plt.title("8 bits")
plt.grid()

plt.subplot(4,1,3)
plt.plot(t,E2)
plt.xlabel("t[s]")
plt.ylabel("error")
plt.title("12 bits")
plt.grid()

plt.subplot(4,1,4)
plt.plot(t,E3)
plt.xlabel("t[s]")
plt.ylabel("error")
plt.title("24 bits")
plt.grid()
plt.show()

