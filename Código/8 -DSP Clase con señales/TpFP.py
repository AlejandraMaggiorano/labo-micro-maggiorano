import sSenoidal as seno
import fpbinary as fp
import matplotlib.pyplot as plt
from scipy.fft import fft
import numpy as np
import math as m
import striangular as triang
import csv

#------------------   1   ------------------
# Desarrollo de algoritmos DSP MATLAB/C:
# Scripts y señales de trabajo:
# ● Leer_Senal_Archivo_Binario.m
# ● save2fileCSV_hex_fixed_1_15.m
# Ejercicios iniciales:
# 1) Sintetizar dos señales en MATLAB utilizando las funciones desarrolladas en el TP2.
# La señal S1 tiene las siguientes características:
# Forma de onda : Senoidal
# Fs = 1000;
# N = 512;
# f0 = 115;
# A = 0.8;
# La señal S2 tiene las sig. características:
# Forma de onda : Senoidal
# Fs = 1000;
# N = 512;
# f0 = 290;
# A = 0.3;

# region Ejercicio 1 - Generación de señales
t1, S1 = seno.sSenoidal(0.8, 115, 0, 512, 1000)
t2, S2 = seno.sSenoidal(0.3, 290, 0, 512, 1000)
# endregion

# region Plot en caso de debug
# plt.subplot(2, 1, 1)
# plt.plot(t1, S1)
# plt.xlabel("t[s]")
# plt.ylabel("p [mmHG]")
# plt.title("Grafico Seno 1")
# plt.grid()
#
# plt.subplot(2, 1, 2)
# plt.plot(t2, S2)
# plt.xlabel("t[s]")
# plt.ylabel("p [mmHG]")
# plt.title("Grafico Seno 2")
# plt.grid()
#
# plt.show()
# endregion


#------------------   2   ------------------

# region Ejercicio 2
# 2)Cuantificar las dos señales utilizando Fi objects en Q7.

def fpgenq(val, q):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=q, signed=True, value=val)
    val_fpsw = fp.FpBinarySwitchable(fp_mode=True, fp_value=val_fp, float_value=val)
    return val_fpsw


def fpsw2hex(val, q):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=q, signed=True, value=val)
    val_fphex = hex(val_fp.__index__())
    return val_fphex

#Convierto a Q7
S1_fp7 = [fpgenq(x, 7) for x in S1]
#print(S1_fp7)
S2_fp7 = [fpgenq(x, 7) for x in S2]

#------------------   3   ------------------

# 3) Realizar una función en MATLAB que me permita convertir una señal en punto fijo a un
# archivo <<sgn.h>>, separando las muestras con coma, guardadas en formato hexadecimal.
# Podremos luego agregar la señal desde C, haciendo:
# int8_t sgn[] = { #include <sgn.h> };

#Convierto a hex
S1_fphex = [fpsw2hex(x, 7) for x in S1]
#print(S1_fphex)
S2_fphex = [fpsw2hex(x, 7) for x in S2]
#Escritura a CSV

np.savetxt("sgn1_q7.h", S2_fphex, fmt='%s', newline =',')
np.savetxt("sgn2_q7.h", S2_fphex, fmt='%s', newline =',')

#------------------   7   ------------------
#Utilizar las señales generadas en los puntos 1) y 2) para sumarlas con las dos funciones del
# punto anterior. Corroborar los resultados con MATLAB.


#Abro archivos
results = []
with open("salida_q7.h") as csvfile:
    reader = csv.reader(csvfile, quoting=csv.QUOTE_ALL)
    for row in reader:
        results= results+row

results2 = []
with open("salida_q15.h") as csvfile:
    reader = csv.reader(csvfile, quoting=csv.QUOTE_ALL)
    for row in reader:
        results2= results2+row

# print (results)
# print (results2)

# Interpreto los datos como hexa
Res_C_fp7_hex = [int(x,16) for x in results]
Res_C_fp15_hex = [int(x.strip(),16) for x in results2]

# print(len(Res_C_fp7_hex))
# print(len(Res_C_fp15_hex))

# Paso a punto fijo los datos de los archivos
Res_C_fp7 = [fpgenq(x, 7) for x in Res_C_fp7_hex]
Res_C_fp15 = [fpgenq(x, 15) for x in Res_C_fp15_hex]

Res_Py_fp15_aux = S1_fp7  # Forma desprolija de asegurarme de que tengo una lista del mismo tamaño

# Realizo la multplicación de mis señales originales en punto fijo.
for index in range(len(S1_fphex)):
    Res_Py_fp15_aux[index] = int(S1_fp7[index])*int(S2_fp7[index])

Res_Py_fp15 = [fpgenq(x, 15) for x in Res_Py_fp15_aux]
Res_Py_fp7 = [fpgenq(x, 7) for x in Res_Py_fp15_aux]

# ----------------------------------------------------------
# Loop de chequeo de errores

Errores15 = Res_Py_fp15 #De nuevo inicialización desprolija
Errores7 = Res_Py_fp7

for index in range(len(Res_Py_fp15)):
    Errores15[index] = int(Res_Py_fp15[index])-int(Res_C_fp15[index])
    Errores7[index] = int(Res_Py_fp7[index]) - int(Res_C_fp7[index])


plt.subplot(1,2, 1)

plt.plot(t1, Errores7, '.k', t1, Errores7, '-b')
plt.title("Q7 - Error - Multiplicación Python - C")
plt.grid()
plt.xlabel("t[s]")

plt.subplot(1,2,2)

plt.plot(t1, Errores15, '.k', t1, Errores15, '-b')
plt.title("Q15 - Error - Multiplicación Python - C")
plt.grid()
plt.xlabel("t[s]")



plt.show()

