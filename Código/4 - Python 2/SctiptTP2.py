import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import signal

# region Ejercicio 1
"""
------EJERCICIO 1
Realizar una función que reciba la amplitud, frecuencia, fase, cantidad de muestras y
frecuencia de muestreo, y devuelva el array de una señal senoidal sintetizada con los
parámetros anteriores. También debe devolver un array de tiempo asociado a la señal. El
prototipo de la función es el siguiente:
function[xt]=sSenoidal(A,f0,ph0,N,Fs)
● Sintetizar utilizando esta función una señal s1 senoidal muestreada a 1 kHz, con
amplitud 1, frecuencia fundamental 100Hz, con 100 muestras. Graficar en tiempo y
frecuencia. Imprimir en el “Command window” la resolución espectral.
● Repetir el punto anterior generando una señal s2 igual que la anterior pero con 128
muestras. ¿Qué diferencias pueden verse?
● Repetir el punto anterior generando una señal s3 igual que la anterior pero con f0 =
145Hz. ¿Qué diferencias pueden verse?
● Repetir el punto anterior generando una señal s4 con f0 = 450 Hz. ¿Que diferencia
puede verse si la f0 = 550 Hz?
"""
def sSenoidal(A, f0, ph0, N , Fs):
    tiempo = np.linspace(0, (1/Fs)*N, num=N)
    senoidal= np.sin(tiempo*f0/(2*np.pi)+ph0)
    senoidal= A*senoidal
    return tiempo, senoidal


tiempo, s1= sSenoidal(1,100,0,100,1000)
tiempo2, s2=sSenoidal(1,100,0,128,1000)
tiempo3, s3=sSenoidal(1,145,0,128,1000)
tiempo4, s4=sSenoidal(1,450,0,128,1000)
tiempo4b, s4b=sSenoidal(1,550,0,128,1000)

plt.subplot(5,1,1)
plt.plot(tiempo,s1)
plt.xlabel("t[s]")
plt.ylabel("S1")
plt.title("Seno 1")
plt.grid()

plt.subplot(5,1,2)
plt.plot(tiempo2,s2)
plt.xlabel("t[s]")
plt.ylabel("S2")
plt.title("Seno 2")
plt.grid()

plt.subplot(5,1,3)
plt.plot(tiempo3,s3)
plt.xlabel("t[s]")
plt.ylabel("S3")
plt.title("Seno 3")
plt.grid()


plt.subplot(5,1,4)
plt.plot(tiempo4,s4)
plt.xlabel("t[s]")
plt.ylabel("S4")
plt.title("Seno 4a")
plt.grid()

plt.subplot(5,1,5)
plt.plot(tiempo4b,s4b)
plt.xlabel("t[s]")
plt.ylabel("S4 b")
plt.title("Seno 4b")
plt.grid()
plt.show()
# endregion

#region Ejercicio 2
"""
Repetir el punto anterior pero para una señal triangular. Utilizar para ello la función
sawtooth().
"""

def sTriangular(A, f0, ph0, N , Fs):
    tiempo = np.linspace(0, (1/Fs)*N, num=N)
    triangular= signal.sawtooth(tiempo*f0/(2*np.pi)+ph0)
    triangular= A*triangular
    return tiempo, triangular


tiempo, s1= sTriangular(1,100,0,100,1000)
tiempo2, s2=sTriangular(1,100,0,128,1000)
tiempo3, s3=sTriangular(1,145,0,128,1000)
tiempo4, s4=sTriangular(1,450,0,128,1000)
tiempo4b, s4b=sTriangular(1,550,0,128,1000)

plt.subplot(5,1,1)
plt.plot(tiempo,s1)
plt.xlabel("t[s]")
plt.ylabel("S1")
plt.title("Triangular 1")
plt.grid()

plt.subplot(5,1,2)
plt.plot(tiempo2,s2)
plt.xlabel("t[s]")
plt.ylabel("S2")
plt.title("Triangular 2")
plt.grid()

plt.subplot(5,1,3)
plt.plot(tiempo3,s3)
plt.xlabel("t[s]")
plt.ylabel("S3")
plt.title("Triangular 3")
plt.grid()


plt.subplot(5,1,4)
plt.plot(tiempo4,s4)
plt.xlabel("t[s]")
plt.ylabel("S4")
plt.title("Triangular 4a")
plt.grid()

plt.subplot(5,1,5)
plt.plot(tiempo4b,s4b)
plt.xlabel("t[s]")
plt.ylabel("S4 b")
plt.title("Triangular 4b")
plt.grid()
plt.show()
# endregion

#Region Ejercicio 3
"""
Realizar un script en el que se sintetice una señal F1 a partir de la siguiente serie de Fourier.
Fs=500Hz %Frecuenciademuestreo
f0=1.67Hz %Frecuenciafundamental
A[0]=-0.670691
A[1]=0.009505 ph[1]=2.775687
A[2]=0.002776 ph[2]=-0.157934
A[3]=0.001028 ph[3]=-1.405200
A[4]=0.000902 ph[4]=-2.791570
A[5]=0.000020 ph[5]=2.517384
Graficar la señal en tiempo y en frecuencia
"""
N=50000
f0=1.67
tiempoFourier,A1= sSenoidal(0.009505, f0, 2.775687, N, 500)
tiempoFourier,A2=sSenoidal(0.002776, 2*f0, -0.157934, N, 500)
tiempoFourier,A3=sSenoidal(0.001028, 3*f0, -1.405200, N, 500)
tiempoFourier,A4=sSenoidal(0.000902, 4*f0, -2.791570, N, 500)
tiempoFourier,A5=sSenoidal(0.000020, 5*f0, 2.517384, N, 500)

A0=-0.670691*np.ones_like(A1)

SFourier =A0 + A1 + A2 + A3 + A4 + A5

#print(A0)
#print(A1)
#print(SFourier)
#print(tiempoFourier)



plt.subplot(2,1,1)
plt.plot(tiempoFourier,SFourier)
plt.xlabel("t[s]")
plt.ylabel("Poliarmónica")
plt.title("Poliarmónica en tiempo")
plt.grid()

plt.subplot(2,1,2)
plt.plot(tiempoFourier,SFourier)
plt.xlabel("t[s]")
plt.ylabel("Poliarmónica")
plt.title("Poliarmónica en frecuencia")
plt.grid()
plt.show()
#endregion
