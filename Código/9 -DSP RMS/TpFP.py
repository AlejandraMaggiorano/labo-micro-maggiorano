import sSenoidal as seno
import fpbinary as fp
import matplotlib.pyplot as plt
from scipy.fft import fft
import numpy as np
import math as m
import striangular as triang

#  ---------- Generación de señales -------------

N = 1024*64

t, S1 = seno.sSenoidal(0.001, 115, 0, N, 1000)
t, S2 = seno.sSenoidal(0.3, 290, 0, N, 1000)
S3= np.ones(N)

# endregion

#------------------   Métodos para pasar a punto fijo  ------------------

# region Pasaje a punto fijo
# Cuantificar las señales y las paso a punto fijo
def fpgenq(val, q):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=q, signed=True, value=val)
    val_fpsw = fp.FpBinarySwitchable(fp_mode=True, fp_value=val_fp, float_value=val)
    return val_fpsw


def fpsw2hex(val, q):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=q, signed=True, value=val)
    val_fphex = hex(val_fp.__index__())
    return val_fphex
# endregion

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

def RMS(S1):
# Convierto a Q15
    S1_fp15 = [fpgenq(x, 15) for x in S1]
    #print(S1_fp15)
# Orden de las operaciones - V1 - Primero cuadrado, luego división por N, luego suma
# Orden de las operaciones - V2 - Primero cuadrado, luego suma, luego división por N
# Orden de las operaciones - V3 - Primero cuadrado, luego división por N, luego sort,  luego suma


# ------------------------ Genero los cuadrados ------------------------
# ----------------------------------------------------------------------

# En punto fijo
    S1_FP_Cuadrado_0 = [x*x for x in S1_fp15]
    S1_FP_Cuadrado = [x.resize((1, 31), overflow_mode=fp.OverflowEnum.sat, round_mode=fp.RoundingEnum.near_pos_inf) for x in S1_FP_Cuadrado_0]

    print(S1_FP_Cuadrado)
    #print(S1_FP_Cuadrado[1].format)

# En punto flotante
    S1_Cuadrado = [x*x for x in S1]

    #print(S1_Cuadrado)

# ------------------------ RMS2 V1 ------------------------
# ---------------------------------------------------------

# Division - Se reutiliza en V3
    S1_FP_V1_Division_0 = [x/N for x in S1_FP_Cuadrado]

    S1_FP_V1_Division = [x.resize((2, 31), overflow_mode=fp.OverflowEnum.sat, round_mode=fp.RoundingEnum.near_pos_inf) for x in
                  S1_FP_V1_Division_0]
    #print(S1_FP_V1_Division)
    print(S1_FP_V1_Division[1].format)
    S1_FLOAT_V1_Division = [x/N for x in S1_Cuadrado]

# Suma

    Suma_S1_V1_FP_0=0
    Suma_S1_V1_FLOAT=0

    for x in S1_FP_V1_Division:
        Suma_S1_V1_FP_0 = Suma_S1_V1_FP_0+x


    Suma_S1_V1_FP = Suma_S1_V1_FP_0.resize((1, 15), overflow_mode=fp.OverflowEnum.sat, round_mode=fp.RoundingEnum.near_pos_inf)

    for x in S1_FLOAT_V1_Division:
        Suma_S1_V1_FLOAT = Suma_S1_V1_FLOAT+x

# ------------------------ RMS2 V2 ------------------------
# ---------------------------------------------------------
    Suma_S1_V2_FP_0 = 0
    Suma_S1_V2_FLOAT = 0

    for x in S1_FP_Cuadrado:
        Suma_S1_V2_FP_0 = Suma_S1_V2_FP_0+x

    for x in S1_Cuadrado:
        Suma_S1_V2_FLOAT =Suma_S1_V2_FLOAT+x

    Suma_S1_V2_FP_0 = Suma_S1_V2_FP_0/N
    Suma_S1_V2_FP = Suma_S1_V2_FP_0.resize((1, 15), overflow_mode=fp.OverflowEnum.sat, round_mode=fp.RoundingEnum.near_pos_inf)
    Suma_S1_V2_FLOAT = Suma_S1_V2_FLOAT/N

# ------------------------ RMS2 V3 ------------------------
# ---------------------------------------------------------
    S1_FP_V1_Division_Ordenada = sorted(S1_FP_V1_Division)
    S1_FLOAT_V1_Division_Ordenada = sorted(S1_FLOAT_V1_Division)

    Suma_S1_V3_FP_0 = 0
    Suma_S1_V3_FLOAT = 0

    for x in S1_FP_V1_Division_Ordenada:
     Suma_S1_V3_FP_0 = Suma_S1_V3_FP_0+x

    Suma_S1_V3_FP = Suma_S1_V3_FP_0.resize((1, 15), overflow_mode=fp.OverflowEnum.sat, round_mode=fp.RoundingEnum.near_pos_inf)

    for x in S1_FLOAT_V1_Division_Ordenada:
        Suma_S1_V3_FLOAT = Suma_S1_V3_FLOAT+x


# ----------------------------------------------------------



# Diferencias entre algoritmos


# Orden de las operaciones - V1 - Primero cuadrado, luego división por N, luego suma
# V1 Mantiene bien escalados los valores para el procesamiento en punto fijo pero puede generarle underflow antes

# Orden de las operaciones - V2 - Primero cuadrado, luego suma, luego división por N
# V2 es el peor para punto fijo, con la meta de generar el overflow más rápido

# Orden de las operaciones - V3 - Primero cuadrado, luego división por N, luego sort,  luego suma
# V3 está hecho para que se note el underflow en punto flotante, la única diferencia entre
# V1 y V3 es un ordenamiento y aún así genera error.


# region Errores
    S1_Error_v1_v2_FP = Suma_S1_V1_FP - Suma_S1_V2_FP
    S1_Error_v1_v2_FLOAT = Suma_S1_V1_FLOAT - Suma_S1_V2_FLOAT

    S1_Error_v1_v3_FP = Suma_S1_V1_FP - Suma_S1_V2_FP
    S1_Error_v1_v3_FLOAT = Suma_S1_V1_FLOAT - Suma_S1_V3_FLOAT

#En punto fijo la diferencia entre ordenado y no ordenado da igual porque la precisión siempre
# es la misma.
    S1_Error_v3_v2_FP = Suma_S1_V3_FP - Suma_S1_V2_FP
    S1_Error_v3_v2_FLOAT = Suma_S1_V3_FLOAT - Suma_S1_V2_FLOAT
# endregion

# region Print
    print("\nDiferencia entre algoritmo con división primero y división al final")
    print("FP:")
    print(S1_Error_v1_v2_FP)
    print("FLOAT:")
    print(S1_Error_v1_v2_FLOAT)

    print("\nDiferencia entre algoritmo con y sin ordenamiento, ambos con división primero")
    print("FP:")
    print(S1_Error_v1_v3_FP)
    print("FLOAT:")
    print(S1_Error_v1_v3_FLOAT)

    print("\n Diferencia entre algoritmo con división al final y división primero con ordenamiento")
    print("FP:")
    print(S1_Error_v3_v2_FP)
    print("FLOAT:")
    print(S1_Error_v3_v2_FLOAT)
# ----------------------------------------------------------------------------
    print("\nV1")
    print("FP:")
    print(Suma_S1_V1_FP)
    print("FLOAT:")
    print(Suma_S1_V1_FLOAT)

    print("\nV2")
    print("FP:")
    print(Suma_S1_V2_FP)
    print("FLOAT:")
    print(Suma_S1_V2_FLOAT)

    print("\n V3")
    print("FP:")
    print(Suma_S1_V3_FP)
    print("FLOAT:")
    print(Suma_S1_V3_FLOAT)
# endregion

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------


RMS(S1)

# Referencia: t, S1 = seno.sSenoidal(0.01, 115, 0, N, 1000)

# El primer caso el algoritmo de punto fijo genera underflow visible en la línea 70,
# después de la división por N. Para esta señal el punto flotante es la única opción ya que
# si bien es representable en punto fijo independientemente del orden de procesamiento el resultado
# dará 0


# RMS(S2)

# Referencia: t, S2 = seno.sSenoidal(0.3, 290, 0, N, 1000)

# Aquí ya estamos en un rango representable cómodamente en punto fijo.
#  El ordenamiento de los números al sumar hace una diferencia para punto flotante donde
#  se nota el underflow durante la suma (los números pequeños se desprecian al ser sumados
#  con los grandes y eso genera el error que se ve entre el valor con y sin ordenamiento)
#  Los bits de guarda intermedios durante la multiplicación y sumatoria en FP evitan un underflow
#  u overflow, lo cual lo hace la representación superior para este caso.

#RMS(S3)

# Referencia: S3= np.ones(N)
# En este caso punto flotante gana ya que Q15 no alcanza para representar un 1 exacto


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------