import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def sSenoidal(A, f0, ph0, N , Fs):
    tiempo = np.linspace(0, (1/Fs)*N, num=N)
    senoidal= np.sin(tiempo*f0/(2*np.pi)+ph0)
    senoidal= A*senoidal
    return tiempo, senoidal