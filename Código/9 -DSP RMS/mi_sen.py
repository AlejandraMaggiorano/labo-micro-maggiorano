import math
import numpy as np


def mi_sen(A, f0, ph0, Fs, N):
    k = np.arange(N)
    Ts = 1 / Fs
    t = k*Ts
    sen = A*np.sin(2*math.pi*f0*t)
    print("Texto para que figure algo:", sen, t)
    return sen, t
