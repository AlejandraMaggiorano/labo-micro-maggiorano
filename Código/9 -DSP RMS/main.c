/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <stdint.h>

#define  size  512 //Esta es la cantidad de muestras, en duro para que no haga lío con storage size

//Fuente del método: https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 
inline int8_t getHigh(int16_t q15_resultado);
inline int8_t getLow(int16_t q15_resultado);
inline int16_t MultiplicarQ15(int8_t q7_v1, int8_t q7_v2);
void MultiplicarArraysQ15(int8_t sgn1[], int8_t sgn2[]);
void MultiplicarArraysQ7(int8_t sgn1[], int8_t sgn2[]);
int main()
{
    int8_t sgn1[size] = { 
    #include "sgn1_q7.h" 
    
};
int8_t sgn2[size] = {
    #include "sgn1_q7.h"    
};
    


//MultiplicarArraysQ7(sgn1, sgn2); 
MultiplicarArraysQ15(sgn1, sgn2); 

    return 0;
}

int16_t MultiplicarQ15(int8_t q7_v1, int8_t q7_v2)
{
    int16_t q15_resultado= q7_v1*q7_v2;
    q15_resultado=q15_resultado<<1;
    return q15_resultado;
}

int8_t getLow(int16_t q15_resultado )
{
    int8_t q7_resultado = (int8_t) q15_resultado; 
    return q7_resultado;
}

int8_t getHigh(int16_t q15_resultado )
{
    int8_t q15_resultado_high = q15_resultado>> 8;
    return q15_resultado_high;
}


void MultiplicarArraysQ15(int8_t  sgn1[],int8_t  sgn2[])
{
    static int16_t sgn_resultado[size];
    for(int i=0;i<size;i++) //Supongo que las señales son igual de largas, si no tendría que poner un try-catch
    {
       sgn_resultado[i]= MultiplicarQ15(sgn1[i],sgn2[i]);
        printf("%X, ",sgn_resultado[i]);
    }
   // return sgn_resultado;
}

void MultiplicarArraysQ7(int8_t  sgn1[], int8_t  sgn2[])
{ 
    int16_t  sgn_resultado[size];
    static int8_t sgn_resultado2[size];
    static int16_t sgn_resultado1[size];
    
    for(int i=0;i<size;i++)
    {
        sgn_resultado1[i]= MultiplicarQ15(sgn1[i],sgn2[i]);
        sgn_resultado2[i]=getHigh(sgn_resultado1[i]);
        printf("%X, ",sgn_resultado2[i]);
    }
   // return sgn_resultado2;
}

/*
int write_to_file(int count, int8_t const *data, char const *fileName)
{
  FILE *f=fopen(fileName,"w");
  if(f==NULL) return -1;
  for (int i=0; i<count; i++)
  {
    fprintf(f,"%d,%s,%f\n",data[i].SomeString, data[i].SomeValue, data[i].SomeSample);
  }
  fclose(f);
  return 0;
}*/
 

