import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import signal


def striangular(A, f0, ph0, N , Fs):
    tiempo = np.linspace(0, (1/Fs)*N, num=N)
    triangular= signal.sawtooth(tiempo*f0/(2*np.pi)+ph0)
    triangular= A*triangular
    return tiempo, triangular