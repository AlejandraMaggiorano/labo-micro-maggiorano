import sSenoidal as seno
import fpbinary as fp
import mi_sen as ms
import matplotlib.pyplot as plt
from scipy.fft import fft
import numpy as np
import math as m
import striangular as triang

#------------------   1   ------------------

# 1) Sintetizar dos señales en MATLAB utilizando las funciones desarrolladas en el TP2.
# La señal S1 tiene las siguientes características:
# Formadeonda:Senoidal
# Fs=1000;
# N=512;
# f0=115;
# A=1;
# La señal S2 tiene las sig. características:
# Formadeonda:Triangular
# Fs=1000;
# N=512;
# f0=290;
# A=1;

t1, S1 = seno.sSenoidal(1, 115, 0, 512, 1000)
t2, S2 = triang.striangular(1, 290, 0, 512, 1000)
#------------------   2   ------------------

# 2) Cuantificar las señales anteriores a través de las funciones “quantize” y
# “quantizer”,haciéndolo en formatos Q7, Q15 y Q31.


def fpgenq(val, q):
    val_fp = fp.FpBinary(int_bits=1, frac_bits=q, signed=True, value=val)
    val_fpsw = fp.FpBinarySwitchable(fp_mode=True, fp_value=val_fp, float_value=val)
    return val_fpsw


# #Convierto a Q7
S1_fp7 = [fpgenq(x, 7) for x in S1]
S2_fp7 = [fpgenq(x, 7) for x in S2]

#Convierto a Q15
S1_fp15 = [fpgenq(x, 15) for x in S1]
S2_fp15 = [fpgenq(x, 15) for x in S2]

#Convierto a Q31
S1_fp31 = [fpgenq(x, 31) for x in S1]
S2_fp31 = [fpgenq(x, 31) for x in S2]
#
# # 3) Realizar la cuantificación Q15 utilizando “fi objects”, con la sintaxis “fi(v, T, F)”. Las
# # multiplicaciones deben guardarse en formato Q31 y las sumatorias en formato Q63. El
# # redondeo es hacia menos infinito ¿qué diferencias hay con la cuantificación realizada en el
# # punto 2?