#include <stdint.h>
#include <stdbool.h>


bool cuentaTiempo(void); //Devuelve true si el tiempo actual coincide con el tiempo de finalización cuentaMax
int32_t inicializarSensores(void); //Inicializa los sensores para simular los que tendría un horno real. Método para simular, no es parte del enunciado.
int32_t encenderHorno(void); //Cambia el horno a encendido
int32_t apagarHorno(void); //Cambia el horno a apagado
bool getEstadoEncendido(void); //Devuelve el estado de encedido o apagado, true para encendido, false para apagado
bool getEstadoPuerta(void); //Devuelve el estado de la puerta, true para abierta, false para cerrada
