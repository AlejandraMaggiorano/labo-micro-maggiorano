/*Implementar el código de la máquina de estados correspondiente al diagrama de estados del horno microondas de la clase 2. Realizar:

- función de máquina de estados con prototipo: int32_t mdeHornoMicroondas (void);

- función de inicialización con prototipo: int32_t InicializarMdeHornoMicroondas (void);

- archivo header de funciones externas: driversExternos.h

Enviar en un archivo NombreApellido_mde1.zip los archivos:

-> hornoMicroondas.c

-> driversExternos.h
 * */

#include <stdint.h>
#include "hornoMicroondas.h"
#include "driversExternos.h"
#include <stdbool.h>
/*
#define  ESTADO_ABIERTO  0
#define  ESTADO_CERRADO  1
#define  ESTADO_ENCENDIDO  2*/
#define  ESTADO_INCIAL 0

typedef enum {ESTADO_ABIERTO, ESTADO_CERRADO,ESTADO_ENCENDIDO} estadosMde;

static estadosMde estadoHorno;

int32_t InicializarMdeHornoMicroondas (void)
{ //Método que ingresa al estado inicial
	estadoHorno = ESTADO_INCIAL;
	inicializarSensores();
}//Inicializar


int32_t mdeHornoMicroondas (void)
{ //Método máquina de estados

	bool a;
switch(estadoHorno)
{
case ESTADO_ABIERTO:
	a = getEstadoPuerta();
	if(a)
		estadoHorno=ESTADO_ABIERTO; //El sensor dice que puertaabierta
	else
		estadoHorno=ESTADO_CERRADO; //El sensor dice que puerta cerrada
	break;

case ESTADO_CERRADO:
	a=getEstadoPuerta();
	if(a)
			estadoHorno=ESTADO_ABIERTO; //El sensor dice que puerta abierta
		else
			{
			a=getEstadoEncendido();
			if(a)
				estadoHorno=ESTADO_ENCENDIDO; //Encendieron el microondas
			else
				estadoHorno=ESTADO_CERRADO; //Todo sigue igual
			}//else - if(a)

	break;

case ESTADO_ENCENDIDO:
	a = getEstadoPuerta();
		if(a)
			estadoHorno=ESTADO_ABIERTO; //Abrieron la puerta, no sigue andando
		else
		{			a=cuentaTiempo();
					if(a)
						estadoHorno=ESTADO_CERRADO; //Terminó
					else
						estadoHorno=ESTADO_ENCENDIDO; //Todavía no termina
					}//else - if(a)
		break;


	break;

default:
	InicializarMdeHornoMicroondas();

}//switch
return 0;
}//mdeHornoMicroondas
