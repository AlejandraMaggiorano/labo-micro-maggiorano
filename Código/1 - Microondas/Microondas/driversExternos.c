/*Este archivo es sólo para propósitos de que el programa realmente pueda
 * ejecutar algo con valores. En un sistema real esto sería proporcionado por el fabricante
 */
#include <stdint.h>
#include <stdbool.h>

static bool hornoAbierto;
static bool botonInicio;
static int32_t cuentaMax;
static int32_t tiempoActual;//No es necesario para el enunciado
static bool hornoEncendido; //No es necesario para el enunciado

int32_t inicializarSensores(void)
{//Inicializa los sensores para simular los que tendría un horno real. Método para simular, no es parte del enunciado.
	tiempoActual =0;
	hornoAbierto= false;
	cuentaMax = 50;  //Claramente no vas a cocinar nada en 50 ticks de CK
	botonInicio = false;
	hornoEncendido=false;
}//InicializarSensores

bool cuentaTiempo(void)
{//Devuelve 0 si el tiempo actual coincide con el tiempo de finalización cuentaMax
tiempoActual++;
if(tiempoActual==cuentaMax)
	return true;
else
	return false;
}//CuentaTiempo

int32_t encenderHorno(void) //Cambia el horno a encendido
{
	hornoEncendido=true;
	return 0;
}//encenderHorno

int32_t apagarHorno(void) //Cambia el horno a apagado
{
	hornoEncendido=false;
	return 0;
}//apagarHorno

bool getEstadoEncendido(void)
{ //Devuelve el estado de encedido o apagado, true para encendido, false para apagado
	return hornoEncendido;

}//getEstadoEncendido

bool getEstadoPuerta(void)
{ //Devuelve el estado de la puerta, true para abierta, false para cerrada
return hornoAbierto;
}//getEstadoPuerta
